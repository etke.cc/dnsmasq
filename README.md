# dnsmasq
[![Matrix](https://img.shields.io/matrix/discussion:etke.cc?logo=matrix&server_fqdn=matrix.org&style=for-the-badge)](https://matrix.to/#/#discussion:etke.cc) [![Buy me a Coffee](https://shields.io/badge/donate-buy%20me%20a%20coffee-green?logo=buy-me-a-coffee&style=for-the-badge)](https://buymeacoffee.com/etkecc)

recursive DNS resolver, read more: [Wikipedia](https://en.wikipedia.org/wiki/Dnsmasq)

[Source code](http://thekelleys.org.uk/gitweb/?p=dnsmasq.git)

## what's included

* [dnsmasq](http://thekelleys.org.uk/gitweb/?p=dnsmasq.git) on alpine linux
* Multiple public recursive resolves used as upstream (`etc/dnsmasq.d`)
* [StevenBlack's hosts](https://github.com/StevenBlack/hosts) file
* weekly automated updates

## what about weekly updates?

Gitlab CI configured to re-run `main` branch every week to download latest alpine linux, dnsmasq and hosts file

## run

```bash
docker run -p 53:5353/udp -p 53:5353/tcp registry.gitlab.com/etke.cc/dnsmasq
```

## where to get

[container registry](https://gitlab.com/etke.cc/dnsmasq/container_registry), [etke.cc](https://etke.cc)
