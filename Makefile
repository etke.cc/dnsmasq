### CI vars
CI_LOGIN_COMMAND = @echo "Not a CI, skip login"
CI_REGISTRY_IMAGE ?= registry.gitlab.com/etke.cc/dnsmasq
REGISTRY_IMAGE ?= registry.etke.cc/etke.cc/dnsmasq
CI_COMMIT_TAG ?= latest
# for main branch it must be set explicitly
ifeq ($(CI_COMMIT_TAG), main)
CI_COMMIT_TAG = latest
endif
# login command
ifdef CI_JOB_TOKEN
CI_LOGIN_COMMAND = @docker login -u gitlab-ci-token -p $(CI_JOB_TOKEN) $(CI_REGISTRY)
endif

# try to get new hosts file, if it fails - just use the file from repo
hosts:
	-wget -O hosts.new https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts && mv hosts.new etc/hosts
	-@rm -f hosts.new

# CI: docker login
login:
	@echo "trying to login to docker registry..."
	$(CI_LOGIN_COMMAND)

# docker build
docker:
	docker build -t ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG} -t ${REGISTRY_IMAGE}:${CI_COMMIT_TAG} .

# docker push
publish:
	docker push ${REGISTRY_IMAGE}:${CI_COMMIT_TAG}
	docker push ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}
