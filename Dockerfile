FROM alpine:latest

#EXPOSE 5353/udp
ENTRYPOINT ["/usr/sbin/dnsmasq"]
CMD ["-k", "--bind-dynamic", "--all-servers", "--log-facility", "/dev/stdout"]

RUN addgroup -S dnsmasq && \
    adduser -S -D -H -h /dev/null -s /sbin/nologin -G dnsmasq -g dnsmasq dnsmasq
RUN apk --no-cache add dnsmasq

COPY ./etc/ /etc/
USER dnsmasq:dnsmasq
